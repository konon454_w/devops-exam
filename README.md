# Описание экзаменационного проекта

## Golang Сервис REST API c хранением состояния в БД PostgreSQL

Внешние зависимости: github.com/prometheus/client_golang/prometheus , github.com/lib/pq

Для подключения к базе данных необходимо определить переменные среды PGHOST, PGPORT, PGUSER, PGPASSWORD, PGDATABASE

Сервис выдает стандартные метрики Golang (взяты из официальной библиотеки Prometheus), а также 2 пользовательские метрики service_requests_total{"code", "method"} и service_db_connections_total{"type", "status", "table"}

Некорректное поведение сервиса обрабатывается различными HTTP-кодами. Их обозначения представлены ниже.

Для 0.0.0.0:8090/metrics (HealthCheck):
- 502 Bad Gateway: Ошибка при открытии SQL-соединения
- 503 Service Unavailable: Ошибка при тестовом пинге базы данных
- список метрик: иначе

Для 0.0.0.0:8090/anypath (anypath != metrics):
- 404 Not found

Для 0.0.0.0:8090/ :
- 204 No Content: Пустые READ-запросы
- 400 Bad Request: Существование / несуществование необходимых полей в таблице БД
- 418 I’m a teapot: Ошибка при парсинге формы (практически неотлавливаема)
- 422 Unprocessable Entity: Ошибка формата (пустое поле, неверное значение и т.д.)
- 500 Internal Server Error: Непредвиденная ошибка или ошибка в регулярном выражении (практически неотлавливаемы)
- 503 Service Unavailable: Ошибка при обработке SQL-запроса

По всему коду делается проверка введенных данных на корректность типа и SQL-инъекции (с помощью проверки RegExpr и экранирования)

При каждом изменении состояния или ошибочном запросе обновляется соответствующая метрика

Если сервис не сможет подключиться к базе данных, вместо метрик он отправляет HTTP-ответ с ошибкой и кодом

Сервис прослушивает порт 8090

Сервис использует БД с двумя таблицами следующей структуры.

Таблица "products":
- ProductId: integer, auto-increment, primary key
- ProductName: char var(255)
- UnitCostRUB: double prec

Таблица "orders":
- OrderId: integer, auto-increment, primary key
- CustomerName: char var(255)
- ProductId: integer, foreign key
- QuantityPerUnit: integer
- TotalCostRUB: double prec

Корректное использование HTML-формы для взаимодействия с сервисом.
- CreateProduct. Required: ProductName,  Cost, RUB
- CreateOrder. Required: CustomerName, ProductName, Quantity. ProductName должен быть в таблице products
- ReadProduct. Required: ProductName
- ReadOrder. Required: CustomerName, ProductName
- UpdateProduct. Required: ProductName,  Cost, RUB. ProductName должен быть в таблице orders
- UpdateOrder. Required: CustomerName, ProductName, Quantity. ProductName должен быть в таблице products; соответствующий ему ProductId - в orders. CustomerName должен быть в таблице orders в той же записи
- DeleteProduct. Required: ProductName
- DeleteOrder. Required: CustomerName, ProductName

**Осторожно, БАГ! Если удалить продукт из таблицы products, который есть в базе orders, записи с этим продуктом не удаляются через предоставленное API**

## Инструкция по ручному развертыванию

0. Установка необходимых приложений: 
- Docker: https://docs.docker.com/engine/install/
- Minikube: https://kubernetes.io/ru/docs/tasks/tools/install-minikube/
- Kubectl: https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/#%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-kubectl-%D0%B2-linux
- KVM: https://minikube.sigs.k8s.io/docs/drivers/kvm2/
- Helm: https://helm.sh/docs/intro/install/

1. Введите в терминале команду (YOUR-DRIVER - драйвер виртуальной машины (в моем случае kvm2), YOUR-NODES-NUM - число виртуальных нод (в моем случае 2)):

<pre>
minikube start --vm-driver="YOUR-DRIVER" --nodes "YOUR-NODES-NUM"
</pre>

2. Включаем аддон Ingress в Minikube:

<pre>
minikube addons enable ingress
</pre>

3. Устанавливаем PostgreSQL в кластер с помощью готового пакета Helm:

<pre>
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install sqldb bitnami/postgresql --set volumePermissions.enabled=true --set primary.service.type=NodePort --set global.postgresql.auth.postgresPassword=q1w2e3r4
</pre>

4. Создаем таблицы, необходимые для сервиса:
<pre>
export POSTGRES_PASSWORD=$(kubectl get secret --namespace default sqldb-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d) && \
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}") && \
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services sqldb-postgresql)
PGPASSWORD="$POSTGRES_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U postgres -d postgres
</pre>

SQL-запросы для создания таблиц:

<pre>
CREATE TABLE "products" (
  "ProductId" serial NOT NULL,
  PRIMARY KEY ("ProductId"),
  "ProductName" character varying(255) NOT NULL,
  "UnitCostRUB" double precision NOT NULL
);

CREATE TABLE "orders" (
  "OrderId" serial NOT NULL,
  PRIMARY KEY ("OrderId"),
  "CustomerName" character varying(255) NOT NULL,
  "ProductId" integer NOT NULL,
  "QuantityPerUnit" integer NOT NULL,
  "TotalCostRUB" double precision NOT NULL
);

ALTER TABLE "orders"
ADD FOREIGN KEY ("ProductId") REFERENCES "products" ("ProductId")
</pre>

5. Настраиваем сервис CRUD-запросов:
<pre>
kubectl apply -f kubernetes/go/
</pre>


6. Настраиваем Ingress:

<pre>
kubectl apply -f kubernetes/ingress.yaml
kubectl get ingress # Ждем появления IP в колонке ADDRESS. Может занять несколько минут
</pre>

В файле /etc/hosts добавить запись ("INGRESS_ADDRESS" - IP из предыдущего шага):

<pre>
"INGRESS_ADDRESS" go.mtsonelove.local
"INGRESS_ADDRESS" prometheus.mtsonelove.local
"INGRESS_ADDRESS" grafana.mtsonelove.local
</pre>

Теперь можно получить доступ к фронтенд-форме сервиса через http://go.mtsonelove.local и к метрикам через http://go.mtsonelove.local/metrics

## Инструкция по настройке мониторинга

1. Создаем Namespace:
<pre>
kubectl create namespace monitoring
</pre>

2. Разворачиваем Prometheus:
<pre>
kubectl apply -f kubernetes/prometheus/
</pre>

Спустя некоторое время (после разворачивания Ingress) Prometheus будет доступен по адресу: http://prometheus.mtsonelove.local

3. Разворачиваем Grafana:
<pre>
kubectl apply -f kubernetes/grafana/
</pre>

Спустя некоторое время (после разворачивания Ingress) Grafana будет доступна по адресу: http://grafana.mtsonelove.local

4. Переходим в Grafana. Импортируем dashboard'ы из файла dashboard.json.

