package main

import (
    "log"
    "net/http"
    "fmt"
    "database/sql"
    "strings"
    "regexp"
    "strconv"
    "os"

    "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/promauto"
    "github.com/prometheus/client_golang/prometheus/promhttp"
    _ "github.com/lib/pq"
)

var (
    host    =   os.Getenv("PGHOST")
    port    =   os.Getenv("PGPORT")
    user    =   os.Getenv("PGUSER")
    password=   os.Getenv("PGPASSWORD")
    dbname  =   os.Getenv("PGDATABASE")

    requests_total = promauto.NewCounterVec(
        prometheus.CounterOpts{
            Name: "service_requests_total",
            Help: "HService requested.",
        },
        []string{"code", "method"})

    db_connections_total = promauto.NewCounterVec(
        prometheus.CounterOpts{
            Name: "service_db_connections_total",
            Help: "Total connections to database.",
        },
        []string{"type", "status", "table"})
)

func SendHTTPError(w http.ResponseWriter, errorCode int, method string, msg string) {
    requests_total.WithLabelValues(strconv.Itoa(errorCode), method).Inc()
    http.Error(w, msg, errorCode)
}

func Escape(sql string) string {
    dest := make([]byte, 0, 2*len(sql))
    var escape byte
    for i := 0; i < len(sql); i++ {
        c := sql[i]

        escape = 0

        switch c {
        case 0:
            escape = '0'
            break
        case '\n':
            escape = 'n'
            break
        case '\r':
            escape = 'r'
            break
        case '\\':
            escape = '\\'
            break
        case '\'':
            escape = '\''
            break
        case '"':
            escape = '"'
            break
        case '\032':
            escape = 'Z'
        }

        if escape != 0 {
            dest = append(dest, '\\', escape)
        } else {
            dest = append(dest, c)
        }
    }

    return string(dest)
}

func StringCheck(str string, req bool, regexpstr string) (ch_str string, err string) {
    str = strings.TrimSpace(str)
    if str == "" {
        if req == true {
            return str, "empty"
        } else {
            return str, ""
        }
    }
    if regexpstr != "" {
        str_matched, str_err := regexp.MatchString(regexpstr, str)
        if str_err != nil {
            return fmt.Sprintf("%v: ", str_err), "re_err"
        }
        if str_matched == false {
            return str, "re_false"
        }
    }
    str = Escape(str)
    return str, ""
}

func CheckError(err error, reqType string, table string) string {
    var status string = "success"
    var errStr string = ""
    if err != nil {
        status = "fail"
        errStr = fmt.Sprintf("%v", err)
    }
    db_connections_total.WithLabelValues(reqType, status, table).Inc()
    return errStr
}

func main() {
    var metricHandleToggle bool = false
    var HTTPCode int = 200
    var errToHttp string

    psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
    db, err := sql.Open("postgres", psqlconn)
    if errStr := CheckError(err, "connection", "none"); errStr != "" {
        metricHandleToggle = true
        HTTPCode = 502
        errToHttp = errStr
    } else {
        defer db.Close()
        err = db.Ping()
        if errStr := CheckError(err, "ping", "none"); errStr != "" {
            metricHandleToggle = true
            HTTPCode = 503
            errToHttp = errStr
        } else {
            fmt.Println("Connected to database!")
        }

    }

    unhealthy := func (w http.ResponseWriter, r *http.Request) {
        SendHTTPError(w, HTTPCode, r.Method, errToHttp)
    }

    if metricHandleToggle {
        http.HandleFunc("/metrics", unhealthy)
    } else {
        http.Handle("/metrics", promhttp.Handler())
    }

    handler := func (w http.ResponseWriter, r *http.Request) {
        if (r.URL.Path != "/") && (r.URL.Path != "/metrics") {
            SendHTTPError(w, 404, r.Method, "404. Not found")
            return
        }
    
        switch r.Method {
        case "GET":		
            http.ServeFile(w, r, "form.html")
            requests_total.WithLabelValues("200", r.Method).Inc()
        case "POST":
            if err := r.ParseForm(); err != nil {
                SendHTTPError(w, 418, r.Method, fmt.Sprintf("ParseForm() err: %v", err))
                return
            }
    
            // fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
                
            crud := r.FormValue("crud")
            entity := r.FormValue("entity")
            productname1 := r.FormValue("productname1")
            unitcostrub := r.FormValue("unitcostrub")
            customername := r.FormValue("customername")
            productname2 := r.FormValue("productname2")
            quantity := r.FormValue("quantity")
    
            crud, crud_err := StringCheck(crud, true, "")
            if crud_err != "" {
                if crud_err == "empty" {
                    SendHTTPError(w, 422, r.Method, "CRUD radio value can't be null")
                    return
                } else {
                    SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", crud_err))
                    return
                }
            }
    
            entity, entity_err := StringCheck(entity, true, "")
            if entity_err != "" {
                if entity_err == "empty" {
                    SendHTTPError(w, 422, r.Method, "ENTITY radio value can't be null")
                    return
                } else {
                    SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", entity_err))
                    return
                }
            }
            
            switch entity {
            case "product":
                productname1, productname1_err := StringCheck(productname1, true, "^[a-zA-Z0-9 ]+$")
                if productname1_err != "" {
                    switch productname1_err {
                    case "empty":
                        SendHTTPError(w, 422, r.Method, "ProductName value cannot be empty")
                        return
                    case "re_err":
                        SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", productname1))
                        return
                    case "re_false":
                        SendHTTPError(w, 422, r.Method, "ProductName must only contain letters, digits and spaces")
                        return
                    default:
                        SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", productname1_err))
                        return
                    }
                }
    
                checkProductExistStmt := fmt.Sprintf(`SELECT "ProductId" FROM "products" WHERE "ProductName"='%s'`, productname1)
                ProdRows, ProdErr := db.Query(checkProductExistStmt)
                if err := CheckError(ProdErr, "select", "products"); err != "" {
                    SendHTTPError(w, 503, r.Method, err)
                    return
                } else {
                    defer ProdRows.Close()
                }

                if (crud == "create") || (crud == "update") {
                    unitcostrub, unitcostrub_err := StringCheck(unitcostrub, true, "^[0-9]+\\.{0,1}[0-9]{0,2}$")
                    if unitcostrub_err != "" {
                        switch unitcostrub_err {
                        case "empty":
                            SendHTTPError(w, 422, r.Method, "Cost, RUB value cannot be empty")
                            return
                        case "re_err":
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", unitcostrub))
                            return
                        case "re_false":
                            SendHTTPError(w, 422, r.Method, "Cost, RUB must be integer or double with 2 digits after dot")
                            return
                        default:
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", unitcostrub_err))
                            return
                        }
                    }

                    if crud == "create" {
                        if ProdRows.Next() {
                            SendHTTPError(w, 400, r.Method, fmt.Sprintf("Product %s already exists", productname1))
                            return
                        }

                        createProdStmt := `insert into "products"("ProductName", "UnitCostRUB") values($1, $2)`
                        _, e := db.Exec(createProdStmt, productname1, unitcostrub)
                        if err := CheckError(e, "insert", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }

                        fmt.Fprintf(w, "%s %s %s with cost %s RUB per unit", crud, entity, productname1, unitcostrub)
                    }
    
                    if crud == "update" {
                        var productid int
                        if ProdRows.Next() {
                            ProdErr = ProdRows.Scan(&productid)
                            if err := CheckError(ProdErr, "scan", "products"); err != "" {
                                SendHTTPError(w, 503, r.Method, err)
                                return
                            }
                        } else {
                            SendHTTPError(w, 400, r.Method, fmt.Sprintf("Product %s does not exist. Nothing to update", productname1))
                            return
                        }

                        updProdStmt := `update "products" set "ProductName"=$1, "UnitCostRUB"=$2 where "ProductId"=$3`
                        _, e := db.Exec(updProdStmt, productname1, unitcostrub, productid)
                        if err := CheckError(e, "update", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }

                        fmt.Fprintf(w, "%s %s %s with cost %s RUB per unit", crud, entity, productname1, unitcostrub)
                    }
                } else {
                    if crud == "read" {
                        readProdStmt := fmt.Sprintf(`SELECT "UnitCostRUB" FROM "products" WHERE "ProductName"='%s'`, productname1)
                        readProdRows, readProdErr := db.Query(readProdStmt)
                        if err := CheckError(readProdErr, "select", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        } else {
                            defer readProdRows.Close()
                        }

                        if readProdRows.Next() {
                            readProdErr = readProdRows.Scan(&unitcostrub)
                            if err := CheckError(readProdErr, "scan", "products"); err != "" {
                                SendHTTPError(w, 503, r.Method, err)
                                return
                            }
                        } else {
                            noUnitMsg := fmt.Sprintf("No match for product %s", productname1)
                            fmt.Fprintf(w, noUnitMsg)
                            SendHTTPError(w, 204, r.Method, "")
                            return
                        }
                        fmt.Fprintf(w, "%s %s %s \nValue: %s", crud, entity, productname1, unitcostrub)
                    }
    
                    if crud == "delete" {
                        var productid int
                        if ProdRows.Next() {
                            ProdErr = ProdRows.Scan(&productid)
                            if err := CheckError(ProdErr, "scan", "products"); err != "" {
                                SendHTTPError(w, 503, r.Method, err)
                                return
                            }
                        } else {
                            SendHTTPError(w, 400, r.Method, fmt.Sprintf("Product %s does not exist. Nothing to delete", productname1))
                            return
                        }

                        deleteProdStmt := `delete from "products" where "ProductId"=$1`
                        _, e := db.Exec(deleteProdStmt, productid)
                        if err := CheckError(e, "delete", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }

                        fmt.Fprintf(w, "%s %s %s", crud, entity, productname1)
                    }
                }
                break
    
            case "order":
                customername, customername_err := StringCheck(customername, true, "^[a-zA-Z0-9 ]+$")
                if customername_err != "" {
                    switch customername_err {
                    case "empty":
                        SendHTTPError(w, 422, r.Method, "CustomerName value cannot be empty")
                        return
                    case "re_err":
                        SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", customername))
                        return
                    case "re_false":
                        SendHTTPError(w, 422, r.Method, "CustomerName must only contain letters, digits and spaces")
                        return
                    default:
                        SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", customername_err))
                        return
                    }
                }
    
                switch crud {
                case "create", "update":
                    productname2, productname2_err := StringCheck(productname2, true, "^[a-zA-Z0-9 ]+$")
                    if productname2_err != "" {
                        switch productname2_err {
                        case "empty":
                            SendHTTPError(w, 422, r.Method, "ProductName value cannot be empty")
                            return
                        case "re_err":
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", productname2))
                            return
                        case "re_false":
                            SendHTTPError(w, 422, r.Method, "ProductName must only contain letters, digits and spaces")
                            return
                        default:
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", productname2_err))
                            return
                        }
                    }
    
                    quantity, quantity_err := StringCheck(quantity, true, "^[1-9][0-9]*$")
                    if quantity_err != "" {
                        switch quantity_err {
                        case "empty":
                            SendHTTPError(w, 422, r.Method, "Quantity value cannot be empty")
                            return
                        case "re_err":
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", quantity))
                            return
                        case "re_false":
                            SendHTTPError(w, 422, r.Method, "Quantity must be valid natural number")
                            return
                        default:
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", quantity_err))
                            return
                        }
                    }

                    quantityInt, quantityParseErr := strconv.Atoi(quantity)
                    if quantityParseErr != nil {
                        SendHTTPError(w, 422, r.Method, "Error while parsing quantity field to integer")
                        return
                    }

                    checkProductExistStmt := fmt.Sprintf(`SELECT "ProductId", "UnitCostRUB" FROM "products" WHERE "ProductName"='%s'`, productname2)
                    ProdRows, ProdErr := db.Query(checkProductExistStmt)
                    if err := CheckError(ProdErr, "select", "products"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    } else {
                        defer ProdRows.Close()
                    }

                    var productid int
                    var unitCost float64
                    if ProdRows.Next() {
                        ProdErr = ProdRows.Scan(&productid, &unitCost)
                        if err := CheckError(ProdErr, "scan", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }
                    } else {
                        SendHTTPError(w, 400, r.Method, fmt.Sprintf("Product %s does not exist. Cannot create an order", productname2))
                        return
                    }

                    var totalCost float64 = unitCost * float64(quantityInt)

                    checkOrderExistStmt := fmt.Sprintf(`SELECT "OrderId" FROM "orders" WHERE "CustomerName"='%s' AND "ProductId"=%d`, customername, productid)
                    OrderRows, OrderErr := db.Query(checkOrderExistStmt)
                    if err := CheckError(OrderErr, "select", "orders"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    } else {
                        defer OrderRows.Close()
                    }

                    if crud == "create" {
                        if OrderRows.Next() {
                            SendHTTPError(w, 400, r.Method, fmt.Sprintf("Order %s by %s already exists", productname2, customername))
                            return
                        }

                        createOrderStmt := `insert into "orders"("CustomerName", "ProductId", "QuantityPerUnit", "TotalCostRUB") values($1, $2, $3, $4)`
                        _, e := db.Exec(createOrderStmt, customername, productid, quantity, totalCost)
                        if err := CheckError(e, "insert", "orders"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }
                        fmt.Fprintf(w, "%s %s %s %s %s\nYour bill: %.2f", crud, entity, customername, productname2, quantity, totalCost)
                    }
    
                    if crud == "update" {
                        var orderid int
                        if OrderRows.Next() {
                            OrderErr = OrderRows.Scan(&orderid)
                            if err := CheckError(OrderErr, "scan", "orders"); err != "" {
                                SendHTTPError(w, 503, r.Method, err)
                                return
                            }
                        } else {
                            SendHTTPError(w, 400, r.Method, fmt.Sprintf("Order %s by %s does not exist. Nothing to update", productname2, customername))
                            return
                        }

                        updOrderStmt := `update "orders" set "QuantityPerUnit"=$1, "TotalCostRUB"=$2 where "OrderId"=$3`
                        _, e := db.Exec(updOrderStmt, quantity, totalCost, orderid)
                        if err := CheckError(e, "update", "orders"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }

                        fmt.Fprintf(w, "%s %s %s %s %s\nTotal bill: %.2f", crud, entity, customername, productname2, quantity, totalCost)
                    }
    
                    break
                
                case "read":
                    productname2, productname2_err := StringCheck(productname2, true, "^[a-zA-Z0-9 ]+$")
                    if productname2_err != "" {
                        switch productname2_err {
                        case "empty":
                            SendHTTPError(w, 422, r.Method, "ProductName value cannot be empty")
                            return
                        case "re_err":
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", productname2))
                            return
                        case "re_false":
                            SendHTTPError(w, 422, r.Method, "ProductName must only contain letters, digits and spaces")
                            return
                        default:
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", productname2_err))
                            return
                        }
                    }

                    checkProductExistStmt := fmt.Sprintf(`SELECT "ProductId" FROM "products" WHERE "ProductName"='%s'`, productname2)
                    ProdRows, ProdErr := db.Query(checkProductExistStmt)
                    if err := CheckError(ProdErr, "select", "products"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    }
                    defer ProdRows.Close()

                    var productid int
                    if ProdRows.Next() {
                        ProdErr = ProdRows.Scan(&productid)
                        if err := CheckError(ProdErr, "scan", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        } 
                    } else {
                        noUnitMsg := "No match found"
                        fmt.Fprintf(w, noUnitMsg)
                        SendHTTPError(w, 204, r.Method, "")
                        return
                    }

                    readOrderStmt := fmt.Sprintf(`SELECT "QuantityPerUnit", "TotalCostRUB" FROM "orders" WHERE "CustomerName"='%s' AND "ProductId"=%d`, customername, productid)
                    readOrderRows, readOrderErr := db.Query(readOrderStmt)
                    if err := CheckError(readOrderErr, "select", "orders"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    } else {
                        defer readOrderRows.Close()
                    }

                    var quantityInt int
                    var totalCostFloat float64
                    if readOrderRows.Next() {
                        readOrderErr = readOrderRows.Scan(&quantityInt, &totalCostFloat)
                        if err := CheckError(readOrderErr, "scan", "orders"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }
                    } else {
                        noUnitMsg := "No match found"
                        fmt.Fprintf(w, noUnitMsg)
                        SendHTTPError(w, 204, r.Method, "")
                        return
                    }
                    fmt.Fprintf(w, "%s %s %s %s\nRead values: Quantity: %d, Bill: %.2f", crud, entity, customername, productname2, quantityInt, totalCostFloat)
                
                    break
                
                case "delete":
                    productname2, productname2_err := StringCheck(productname2, true, "^[a-zA-Z0-9 ]+$")
                    if productname2_err != "" {
                        switch productname2_err {
                        case "empty":
                            SendHTTPError(w, 422, r.Method, "ProductName value cannot be empty")
                            return
                        case "re_err":
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("RegularExpression error: %s ", productname2))
                            return
                        case "re_false":
                            SendHTTPError(w, 422, r.Method, "ProductName must only contain letters, digits and spaces")
                            return
                        default:
                            SendHTTPError(w, 500, r.Method, fmt.Sprintf("Unexpected error: %v ", productname2_err))
                            return
                        }
                    }

                    checkProductExistStmt := fmt.Sprintf(`SELECT "ProductId" FROM "products" WHERE "ProductName"='%s'`, productname2)
                    ProdRows, ProdErr := db.Query(checkProductExistStmt)
                    if err := CheckError(ProdErr, "select", "products"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    }
                    defer ProdRows.Close()

                    var productid int
                    if ProdRows.Next() {
                        ProdErr = ProdRows.Scan(&productid)
                        if err := CheckError(ProdErr, "scan", "products"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        } 
                    } else {
                        SendHTTPError(w, 422, r.Method, fmt.Sprintf("Product %s does not exist. Nothing to delete", productname2))
                        return
                    }

                    checkOrderExistStmt := fmt.Sprintf(`SELECT "OrderId" FROM "orders" WHERE "ProductId"=%d AND "CustomerName"='%s'`, productid, customername)
                    checkOrderExistRows, checkOrderExistErr := db.Query(checkOrderExistStmt)
                    if err := CheckError(checkOrderExistErr, "select", "orders"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    } else {
                        defer checkOrderExistRows.Close()
                    }

                    var orderid int
                    if checkOrderExistRows.Next() {
                        checkOrderExistErr = checkOrderExistRows.Scan(&orderid)
                        if err := CheckError(checkOrderExistErr, "scan", "orders"); err != "" {
                            SendHTTPError(w, 503, r.Method, err)
                            return
                        }
                    } else {
                        SendHTTPError(w, 400, r.Method, fmt.Sprintf("Order %s by %s does not exist. Nothing to delete", productname2, customername))
                        return
                    }

                    deleteOrderStmt := `delete from "orders" where "OrderId"=$1`
                    _, e := db.Exec(deleteOrderStmt, orderid)
                    if err := CheckError(e, "delete", "orders"); err != "" {
                        SendHTTPError(w, 503, r.Method, err)
                        return
                    }
    
                    fmt.Fprintf(w, "%s %s %s %s", crud, entity, customername, productname2)
                    break
    
                default:
                    requests_total.WithLabelValues("422", "root", r.Method).Inc()
                    http.Error(w, fmt.Sprintf("Wrong CRUD value: %v", crud), 422)
                    return
                }
    
                break
    
            default:
                requests_total.WithLabelValues("422", "root", r.Method).Inc()
                http.Error(w, fmt.Sprintf("Wrong ENTITY value: %v", entity), 422)
                return
            }
        }
    }
    
    http.HandleFunc("/", handler)
    fmt.Println("Server is listening...")
    log.Fatal(http.ListenAndServe(":8090", nil))
}